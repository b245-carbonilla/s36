// dependencies
const express=require('express');
const mongoose=require('mongoose');

// app using express function
const app = express();
const port = 4000;

const taskRoutes = require("./routes/taskRoutes");

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.wqnqsck.mongodb.net/s36?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the databaase!"));

app.use("/tasks", taskRoutes);

app.listen(port, () => console.log(`Now listening to port ${port}`));

