const express=require('express');
const router=express.Router();
const taskController=require("../controller/taskController")

router.get("/", (req, res) => {
    taskController.getAllTasks().then(
        resultFromController => res.send(
            resultFromController
        ))})
router.post("/", (req, res) => {
    taskController.createTask(req.body)
    .then(resultFromController => 
        res.send(resultFromController));
    })

router.put("/:id/complete", (req, res) => {
    taskController.updateTask(req.params.id, req.body)
    .then(resultFromController=>res.send(resultFromController))
})
module.exports = router;

