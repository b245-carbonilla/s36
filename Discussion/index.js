//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
//This allows us to use all the routes defined in "taskRoute.js"
const taskRoutes = require("./routes/taskRoutes");

//Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extented:true}));

//Database connection
//Connecting to MongoDB Atlas
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.wqnqsck.mongodb.net/s36?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the databaase!"));

//Add the task route
//Allows all the task routes created in the "taskRoutes.js" file to use "/tasks"
app.use("/tasks", taskRoutes);
//http://localhost:4000/tasks

app.listen(port, () => console.log(`Now listening to port ${port}`));
