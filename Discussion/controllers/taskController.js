//Controllers contains the functions and business logic of our Express JS application
// Meaning all the operations it can do will be place in this file
//Uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations
//Allows us  to use the content of the "task.js" file in the "models" folder
const Task = require("../models/task");

//Controller function for GETTING ALL THE TASKS
//Defines the functions to be used in the "taskRoutes.js" and also we export these functions
module.exports.getAllTasks = () => {

	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman

	//model.mongoose mthod
	return Task.find({}).then(result => {

		return result;
	})

}

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	//the first parameter will store the result return by the Mongoose save method
	//the second parameter will store the "error" object
	return newTask.save().then((task, error) => {

		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {

		if(error) {
			console.log(error)
			return false
		} else {

			return removedTask
		}
	}) 
}


module.exports.updateTask = (taskId, reqBody) => {

	return Task.findById(taskId).then((result, error) => {

		if(error) {
			console.log(error)
			return false
		} 

		result.name = reqBody.name;

		return result.save((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
			} else {
				return updatedTask
			}
		})
	}) 
}